// 18HW.cpp : This file contains the 'main' function. Program execution begins and ends there.
//

#include <iostream>
using namespace std;

class Stack
{
public:
	float Data;
	Stack* next;
	void Pop(Stack** top)
	{
		Stack* q = *top;
		if (q == *top) {//���� ����� ��������� ����� �������, �� ���� �������, ������� ��� ����� ������� - �������
			*top = q->next;//����������� ������� �� ��������� �������
			free(q);//������� ������
			q->Data = NULL; //����� �� ��������� ������ �� �������� ���������� � ��������� ������, ��� ��� � ��������� ������������ ��������� ������ ����� ���������� �� NULL ��������, � �������� "����� ������ ����������" ��� �����  "-2738568384" ��� ������, � ����������� �� �����������.
			q->next = NULL;
		}

		//���������� ������� ������ ��� ����������
		q = q->next;
	}

	void Push(Stack** top, int d)
	{
		Stack* q ;
		q = new Stack;
		q->Data = d; //���������� D � Data ��������
		if (top == NULL) { //���� ������� ���, ������ ���� ������
			*top = q; //�������� ����� ����� ����� �������
		}
		else //���� ���� �� ������
		{
			q->next = *top; //�������� ����� �� ������ ��������, � �������. ������ ������ ������ �� ������� ������.
			*top = q; //�����, ��� �������� ������ �������� ����� �������
		}
	}


	void Show(Stack* top)
	{
		Stack* q = top;//   q 
		while (q)
		{
			cout << " " << q->Data;//    
			q = q->next;//    q   
		}
	}

private:

	float q = 0;
	float top = 0;
};

int main()
{
	Stack* top = NULL;
	Stack* stackFloat = new Stack();
	stackFloat->Push(&top, 5);
	stackFloat->Push(&top, 2);
	stackFloat->Push(&top, 3);
	stackFloat->Push(&top, 4);
	stackFloat->Push(&top, 1);
	stackFloat->Push(&top, 6);
	stackFloat->Push(&top, 7);
	stackFloat->Push(&top, 8);

	stackFloat->Show(top);
	cout << endl;
	stackFloat->Pop(&top);
	stackFloat->Pop(&top);
	stackFloat->Pop(&top);
	stackFloat->Show(top);
	cout << endl;
	delete stackFloat;

}


// Run program: Ctrl + F5 or Debug > Start Without Debugging menu
// Debug program: F5 or Debug > Start Debugging menu

// Tips for Getting Started: 
//   1. Use the Solution Explorer window to add/manage files
//   2. Use the Team Explorer window to connect to source control
//   3. Use the Output window to see build output and other messages
//   4. Use the Error List window to view errors
//   5. Go to Project > Add New Item to create new code files, or Project > Add Existing Item to add existing code files to the project
//   6. In the future, to open this project again, go to File > Open > Project and select the .sln file
